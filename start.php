<!DOCTYPE html>
<html>
<head>
<?php require 'bootstrap.php';?>
</head>
<style>
input,label,button{
	width:100%;
}
input{
	padding:5px;
	border-radius:4px;
}
h4{
	text-align:center;
	margin:10px;
}
</style>
<body>
<div class="container" style="padding:10%; width:50%">
	<div class="card">
		<div class="card-header"><h4>Elektroniczny dziennik</h4></div>
		<div class="card-body" style="padding:10%">
			<form autocomplete="off">
				<div class="form-group">
					<label for="text">Login:</label>
					<input type="text" placeholder="Wpisz login">
				</div>
				<div class="form-group">
					<label for="pwd">Hasło:</label>
					<input type="password" id="pwd" placeholder="Wpisz hasło">
				</div>	
				<button class="btn btn-primary">Zaloguj</button>
			</form> 
		</div>
	</div>
</div>
</body>
</html>