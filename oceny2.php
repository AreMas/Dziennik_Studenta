<!DOCTYPE html>
<html>
 <title>Wykładowca</title>
  <meta charset="utf-8">
<head>
<?php require 'bootstrap.php';?>
</head>
<style>
.txmid{
text-align:center;
}
#mp0{
	padding:0px;
	margin:0px;
	}
</style>
<body id="fullheight">
<div class="container" style="padding: 25px">
	<div class="card">	<!--body-->
		<div class="card-header">	<!--head-->
			<div class="row" style="padding:10px">
				<div class="col">
				<p style="font-size:150%;margin:0px">Wykładowca</p>
				</div>
				<div class="col"><button type="button" class="btn btn-link" style="float:right"><a href="logout.php">Wyloguj się</a></button></div>
			</div>
		</div>
		<div class="card-body">	<!--main-->
			<div class="card">
			<div class="card-header">Student</div>
				<table class='table-striped table-hover' style='width:100%'>
					<tr>
						<th >Przedmiot</th>
						<th class='txmid'>Ocena 1
						<th class='txmid'>Ocena 2
						<th class='txmid'>Ocena 3
					<tr>
						<td>Przedmiot1</td>
						<td class='txmid'>1
						<td class='txmid'>2
						<td class='txmid'>3
					<tr>
						<td>Przedmiot2</td>
						<td class='txmid'>1
						<td class='txmid'>2
						<td class='txmid'>3
					<tr>
						<td>Przedmiot3</td>
						<td class='txmid'>1
						<td class='txmid'>2
						<td class='txmid'>3
				</table>
			</div>
		</div>
<div class="card-footer">	<!--footer-->

	<div class="form-group" id="mp0">
	
		<form id="mp0" name ="wstaw" method="post">
			<div class="row" id="mp0">
				<div class="col-4" id="mp0">
					<select class="form-control" name="przedmiot">
						<option value="0" disabled selected>Przedmiot</option>
						<option value="1">przedmiot1</option>
						<option value="2">przedmiot2</option>
						<option value="3">przedmiot3</option>
					</select></div>
				<div class="col-2" id="mp0">
					<select class="form-control" name="ocena1">
						<option value="0" disabled selected>Ocena1</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
					</select></div>
				<div class="col-2" id="mp0">
					<select class="form-control" name="ocena2">
						<option value="" disabled selected>Ocena2</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
					</select></div>
				<div class="col-2" id="mp0">
					<select class="form-control" name="ocena3">
						<option value="" disabled selected>Ocena3</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
					</select></div>
				<div class="col-2" id="mp0">
					<input class="btn btn-primary" type="submit" name="wstaw" value="wybierz" style="width:100%"/>
				</div>
			</div>
		</form>
		
		<div class="row-12" style="padding:10px 0px 10px 0px">
			<ul class="breadcrumb" style="margin:0px">
				<li><a href="studenci.php">Studenci</a></li>/
				<li>Student</li>
			</ul>
		</div>
	</div>
</div>
</body>
</html>	