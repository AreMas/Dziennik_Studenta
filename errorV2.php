<?php
require 'bootstrap.php';
?>
<script>
function goBack() {
    window.history.back();
}
</script>
<div class='container-fluid'>
	<div class='alert alert-danger' role='alert' style='padding:5%; margin:12.5% 25%'>
		<div class='container-fluid'>
			<h1 style='text-align:center'>Błąd</h1>
			<button onclick="goBack()" type="button" class="btn btn-link btn-block"><p style='text-align:center'>Wróć i popraw dane</p></button>
		</div>
	</div>
</div>