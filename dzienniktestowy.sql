-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 07 Kwi 2018, 10:59
-- Wersja serwera: 10.1.28-MariaDB
-- Wersja PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `dziennik`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dane`
--

CREATE TABLE `dane` (
  `ID` int(11) NOT NULL,
  `P_ID` int(11) NOT NULL,
  `Ocena` int(11) NOT NULL,
  `K_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `dane`
--

INSERT INTO `dane` (`ID`, `P_ID`, `Ocena`, `K_ID`) VALUES
(2, 1, 5, 1),
(3, 1, 3, 1),
(4, 1, 4, 1),
(5, 1, 2, 1),
(2, 2, 3, 1),
(3, 2, 2, 1),
(4, 2, 4, 1),
(5, 2, 3, 1),
(2, 3, 2, 1),
(3, 3, 4, 1),
(4, 3, 3, 1),
(5, 3, 5, 1),
(2, 4, 5, 1),
(3, 4, 3, 1),
(4, 4, 2, 1),
(5, 4, 4, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dane logowania`
--

CREATE TABLE `dane logowania` (
  `ID` int(11) NOT NULL,
  `Imie` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `Nazwisko` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `Login` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `Password` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `Mail` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `Grupa` int(11) NOT NULL,
  `Activate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `dane logowania`
--

INSERT INTO `dane logowania` (`ID`, `Imie`, `Nazwisko`, `Login`, `Password`, `Mail`, `Grupa`, `Activate`) VALUES
(1, 'Admin', 'Admin', 'Admin', '12345', 'qwertyuiop@qwe.pl', 1, 1),
(2, 'Arkadiusz', 'Maszczyk', 'aremas', '1234', 'asdfgh@jkhl.pl', 3, 1),
(3, 'Mateusz', 'Domanski', 'Doman', '12356', 'zxcvbn@bnm.pl', 3, 1),
(4, 'Mariusz', 'Borisow', 'king', '123567', 'ztrewq@bnm.pl', 3, 1),
(5, 'Mateusz', 'Domanski', 'doman', '12356', 'zxcvbn@bnm.pl', 3, 1),
(6, 'Aleksandra', 'Terabin', 'a.terabin', '12345', 'a.terabin@edu.pl', 2, 1),
(7, 'Monika', 'Ratownik', 'm.ratownik', '1235', 'm.ratownik@edu.pl', 2, 1),
(8, 'Robert', 'Bierze', 'r.bierze', '1236', 'r.bierze@edu.pl', 2, 0),
(9, 'Przemysław', 'Fajny', 'p.fajny', '4356', 'p.fajny@edu.pl', 2, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przedmiot`
--

CREATE TABLE `przedmiot` (
  `P_ID` int(11) NOT NULL,
  `Przedmiot` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `przedmiot`
--

INSERT INTO `przedmiot` (`P_ID`, `Przedmiot`) VALUES
(1, 'Symulacje Komputerowe'),
(2, 'Programowanie Rozproszone'),
(3, 'In?ynieria Oprogramowania'),
(4, 'Projekt Zespo?owy'),
(5, 'Proseminarium'),
(6, 'Grafika i Komunikacja Cz?owiek Komputer'),
(7, 'Automatyka i Robotyka'),
(8, 'Interpretowane Jezyki Programowania'),
(9, 'Rzywienie Cz?owieka'),
(10, 'Chemia Organiczna'),
(11, 'Marketing');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `student`
--

CREATE TABLE `student` (
  `K_ID` int(11) NOT NULL,
  `Kierunek` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `student`
--

INSERT INTO `student` (`K_ID`, `Kierunek`) VALUES
(1, 'Informatyka'),
(2, 'Dietetyka'),
(3, 'Administracja'),
(4, 'Grafika'),
(5, 'Turystyka i Rekreacja'),
(6, 'Kosmetologia'),
(7, 'Stosunki Miedzynarodowe'),
(8, 'Zarządzanie');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `dane`
--
ALTER TABLE `dane`
  ADD KEY `ID` (`ID`,`P_ID`),
  ADD KEY `K_ID` (`K_ID`),
  ADD KEY `dane_ibfk_1` (`P_ID`);

--
-- Indexes for table `dane logowania`
--
ALTER TABLE `dane logowania`
  ADD KEY `ID` (`ID`);

--
-- Indexes for table `przedmiot`
--
ALTER TABLE `przedmiot`
  ADD KEY `P_ID` (`P_ID`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD KEY `K_ID` (`K_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
